package com.example.mykotlinproject

import java.math.BigDecimal
import java.time.DayOfWeek
import java.time.LocalDateTime

class OrdersAnalyzer {
    data class Order(val orderId: Int, val creationDate: LocalDateTime, val orderLines: List<OrderLine>)

    data class OrderLine(val productId: Int, val name: String, val quantity: Int, val unitPrice: BigDecimal)

    fun totalDailySales(orders: List<Order>): Map<DayOfWeek, Int> {

        val resultMap = mutableMapOf<DayOfWeek, Int>()

        for (i in 0 until orders.size) {
            if(!orders[i].orderLines.isEmpty()){
                var tempQuantityCount = 0
                if(!resultMap.containsKey(orders[i].creationDate.dayOfWeek)){
                    resultMap.put(orders[i].creationDate.dayOfWeek, 0)
                }else{
                    tempQuantityCount = resultMap.get(orders[i].creationDate.dayOfWeek)!!
                }
                for(j in 0 until orders[i].orderLines.size) {
                    tempQuantityCount += orders[i].orderLines[j].quantity
                }
                resultMap[orders[i].creationDate.dayOfWeek]=tempQuantityCount
            }
        }
        return resultMap
    }

}